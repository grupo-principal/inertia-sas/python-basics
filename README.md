# Python Basics

## Herramientas
### Instalación en Mac
#### Consola
1. Abre la carpeta Aplicaciones. Luego, ve a Utilidades y haz doble click en la aplicación Terminal. Esto debería abrirte la consola de comandos. Si no te funcionó o no encontraste la aplicación de Terminal, puedes pulsar ⌘ + barra espaciadora para abrir Spotlight. Allí escribe “Terminal” y haz click en el primer resultado de búsqueda.
2. Ejecuta los siguientes comandos:
````
sudo xcode-select --install
sudo xcode-select --reset
````
Ingresa la contraseña de administrador.

#### Editor de código
1. Abre tu navegador preferido (Safari, Chrome, el que quieras) y dirígete a https://code.visualstudio.com/.
1. Una vez allí, haz click en el botón “Download for Mac” o “Descargar para Mac”.
1. Abre la lista de archivos descargados de tu navegador, y encuentra el instalador.
1. Arrastra el archivo Visual Studio Code.app a la carpeta Aplicaciones.
1. Añade el editor al dock dándole click derecho al ícono que te aparece en pantalla y seleccionando “Options” u “Opciones”, y luego “Keep in dock” o “Mantener en el dock”

#### Python
El lenguaje de programación es la joya de la corona de nuestras herramientas. Sin Python no puedes programar, es así de simple. Sigue los siguientes pasos:

1. Abre tu navegador preferido (Safari, Chrome, el que quieras) y dirígete a https://www.python.org/downloads/
1. Da click en el botón “Download Python 3.x.x”. En las “x” vas a ver números. Lo importante es que el primer número sea un 3, los dos que siguen no nos interesan, porque cambian todo el tiempo.
1. Abre el instalador descargado, y sigue los pasos.

### Instalación en Ubuntu
#### Consola
En Ubuntu no necesitas instalar una consola a diferencia de Windows. Para poder usar la terminal debes presionar `Ctrl + Alt + t`.

#### Editor de código
1. Abre tu navegador preferido (Firefox, Chrome, el que quieras) y dirígete a https://go.microsoft.com/fwlink/?LinkID=760868
1. Abre el archivo descargado. Notarás que termina en “.deb”
1. Sigue los pasos de instalación. 
    - Introduce la contraseña de tu sistema.
    - Espera a que termine la instalación.
    - Instalación finalizada.
1. Abre tu editor.

#### Python
El lenguaje de programación es la joya de la corona de nuestras herramientas. Sin Python no puedes programar, es así de simple. Sigue los siguientes pasos:
1. Abre tu terminal y ejecuta los siguientes comandos:

    ````
    sudo apt update
    sudo apt install python3-pip
    ````
    Te mostrará si deseas continuar con la instalación, da click en enter. La instalación empezará.
1. Ejecuta el comando python3 -V para verificar que el lenguaje de programación se instaló correctamente

### Fuentes
- [Python](https://www.python.org/)
- [cmder - Portable console emulator for Windows](https://cmder.net/)
- [Visual Studio Code](https://code.visualstudio.com/)

---

## Operadores Aritméticos
### Iniciar consola interactiva
````
python3
````
### Operaciones
#### Suma
````py
>>> 5 + 5
10
````

````py
>>> print(5 + 5)
10
````
#### Resta
````py
>>> 5 - 5
0
````
#### Multiplicación
````py
>>> 2 * 5
10
````

````py
>>> 5 * 4 * 3 * 2 * 1 
120
````
#### División
````py
>>> 21 / 5
4.2
````
#### División Entera
````py
>>> 21 // 5
4
````
#### Módulo (Resto División)
````py
>>> 21 % 5
1
````
#### Potencia
````py
>>> 2 ** 3
8
````

#### Orden Operaciones
Primero multiplicación, luego suma
````py
>>> 5 + 5 * 2
15
````
Equivalente a 
````py
>>> 5 + (5 * 2)
15
````
Para hacer primero la suma se debe indicar con paréntesis
````py
>>> (5 + 5) * 2
20
````

### Fuentes
- [Using the Python Interpreter](https://docs.python.org/3/tutorial/interpreter.html)
- [An Informal Introduction to Python](https://docs.python.org/3/tutorial/introduction.html)
- [Python Arithmetic Operators](https://www.w3schools.com/python/gloss_python_arithmetic_operators.asp)
- [Python Keywords](https://www.w3schools.com/python/python_ref_keywords.asp)

---

## Variables
Es un lugar en memoria (una especie de caja) en el que podemos guardar objetos (números, texto, etc). Esta variable posee un identificador o nombre con el cual podemos llamarla cuando la necesitemos.

- Asignación de variables
    En python creamos las variables asignándoles un valor de la siguiente manera:
    ````
    <identificador> = <valor>
    ````
    en este caso el signo = se lee como “asignar”
- Reasignación de variables
    Podemos en cualquier momento cambiar el valor de nuestra variable volviendo a asignar un valor al mismo identificador
    ````
    <identificador> = <nuevo_valor>
    ````
### Asignación
````py
>>> numero = 3
````
### Imprimir Valor Variable
Desde la consola
````py
>>> print(numero)
3
````
Desde el editor
````py
>>> numero
3
````
#### Suma Variables
````py
>>> numero1 = 5
>>> numero2 = 6
>>> numero1 + numero2
11
````
#### Reasignar Variables
````py
>>> numero1 = 12
>>> numero1
12
>>> numero1 + numero2
18
````
#### Restar Variables
````py
>>> numero1 - numero2
6
````

#### Guardar Resultado en Variable
````py
>>> numero_resultado = numero1 + numero2
>>> numero_resultado
18
````

### Reglas en el uso de identificadores

- No pueden empezar con un número.
- Deben estar en minúsculas
- Para separar las palabras usamos el guion bajo: _
- Estas reglas son aplicadas al lenguaje python, en otros lenguajes pueden haber otras reglas.

### Fuentes
- [An Informal Introduction to Python](https://docs.python.org/3/tutorial/introduction.html)
- [Python Variables](https://www.w3schools.com/python/python_variables.asp)

---

## Datos primitivos
### Objetos
Un objeto es una forma de modelar el mundo, en los lenguajes de programación se caracterizan por tener métodos y atributos. En python todo es un objeto.

### Tipos de datos
Podemos encontrar cuatros tipo de datos que vienen definidos por defecto en python, a estos tipos de datos los conocemos como primitivos.

- Integers: Números Enteros
- Floats: Números de punto flotantes (decimales)
- Strings: Cadena de caracteres (texto)
- Boolean: Boolenaos (Verdadero o Falso)

Algunos operadores aritméticos pueden funcionar para operar con otros tipos de datos, por ejemplo: podemos sumar strings, lo que concatena el texto o multiplicar un entero por un strings lo que repetirá el string las veces que indique el entero
### Fuentes
- [An Informal Introduction to Python](https://docs.python.org/3/tutorial/introduction.html)
- [Python Data Types](https://www.w3schools.com/python/python_datatypes.asp)